def nombre_contig_ospf_zone_acces_multiple(nb_interfaces):
  """
  Fonction python qui permet de déterminer le nombre de contiguïté de voisinage qu’un routeur OSPF doit avoir dans une zone à accès multiple
  
  Args:
    nb_interfaces (int): Nombre d'interfaces du routeur dans la zone d'accès multiple.

  Returns:
    int: Nombre de contiguïtés de voisinage requises.
  """

  if nb_interfaces <= 1:
    # Cas limite : 1 interface ou moins, ou encore aucune contiguïté requise
    return 0
  else:
    # Formule de calcule du nombre de contiguïtés requises (n-1)
    return nb_interfaces - 1

# Un Exemple d'utilisation de cas
nombre_interfaces = 8
nb_contig_requis = nombre_contig_ospf_zone_acces_multiple(nombre_interfaces)
print(f"Pour {nombre_interfaces} interfaces, un routeur OSPF dans une zone à accès multiple requiert {nb_contig_requis} contiguïtés de voisinage.")
